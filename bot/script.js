
function postHumain(str) {
	var x = document.createElement("LI");
	var t = document.createTextNode("Vous : "+str);
	x.appendChild(t);
	document.getElementById("myList").appendChild(x);
	x.style.color = "red";
	var e = document.getElementById("scrollable");
	e.scrollTop = e.scrollHeight;
}

function postBot(str) {
	var x = document.createElement("LI");
	var t = document.createTextNode("Bot : "+str);
	x.appendChild(t);
	document.getElementById("myList").appendChild(x);
	x.style.color = "blue";
	var e = document.getElementById("scrollable");
	e.scrollTop = e.scrollHeight;
}

function envoyerMsg()
{
	var msg = document.getElementById("inputMsg").value;
	console.log(msg);
	$.ajax({
		url : 'actions.php',
		type : 'POST', 
		data : 'msg='+msg,
		dataType : 'html',
		success: function(d){
			postBot(d);
		}
	 });
	 postHumain(msg);
	 document.getElementById("inputMsg").value = "";
	 return false;
}