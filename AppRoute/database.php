<?php

$servername = "localhost";
$username = "root";
$password = "";

// Create connection
$db = new PDO("mysql:host=$servername;dbname=approute", $username, $password);

function insertDemande($db,$nom,$lat,$lng){
  cleanDemande($db);
  $req = $db->prepare('
  INSERT INTO demande
  (nom,lat,lng,date)
  VALUES
  (:nom , :lat , :lng , CURRENT_TIMESTAMP);
  ');
  $req->execute(array(
    "nom" => $nom,
    "lat" => $lat,
    "lng" => $lng
  ));
}

function getDemande($db){
  cleanDemande($db);
  $req = $db->prepare('
  SELECT * FROM demande;
  ');
  $req->execute(array());
  return $req;
}

function cleanDemande($db){
  $req = $db->prepare('
  DELETE FROM demande WHERE current_timestamp-date>1800;
  ');
  $req->execute(array());
}


cleanDemande($db);
?>
