<!DOCTYPE html>
<html lang="en">
<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <title>Bouton d'aide en cas d'urgence</title>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="bouton.css" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="text-center">
  <h1>Avez vous besoin d'aide ?</h1>
</div>

<!--premier bouton-->

<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <div class="contour1">
        <a href="optaide.html">
        <div class="rond1">
          OUI
        </div>
       </a>
      <div class="degrade">
    </div>
  </div>
</div>


<div class="text-center">
  <h3>Sinon vous cherchez à rendre service ?</h3>
</div>

<!--deuxième bouton-->

<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <div class="contour2">
        <a href="listeDemandes.php">
         <div class="rond2">
          Cliquer ici
         </div>
        </a>
      <div class="degrade">
    </div>
  </div>
</div>


</body>
</html>
