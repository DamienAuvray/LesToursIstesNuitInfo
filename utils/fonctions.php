<?php
    function cleanString($iStr)
    {
        $oStr = $iStr;
        $oStr = trim($iStr);
        $oStr = htmlspecialchars($oStr,ENT_QUOTES);
        $oStr = stripslashes($oStr);
        return $oStr;
    }

    function SQL_IN($array)
    {
        $i = count($array);
        if($i > 0)
        {
            $str = "'".$array[0]."'";
            for($j = 1; $j < $i; $j++)
            {
                if(strcmp($array[$j],'')!=0)
                    $str=$str.",'".$array[$j]."'";
            }
        }
        return $str;
    }

    function mdArrayCount($array)
    {
        $i=0;
        foreach($array as $ar)
        {
            $i++;
        }
        return $i;
    }
?>
