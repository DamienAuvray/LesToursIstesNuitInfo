<?php
    /// <INCLUDE AND PRECONFIG
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    header('Content-Type: text/plain');
    include("fonctions.php");
    /// INCLUDE AND PRECONFIG>

    /// <CLASSES
    abstract class TypesPhrases
    {
        const Inconnu = 0;
        const Question = 1;
        const Normal = 2;
    }

    abstract class TypesPlur
    {
        const Inconnu = 0;
        const Solo = 1;
        const Multiple = 2;
    }

    abstract class TypesSexe
    {
        const Inconnu = 0;
        const Homme = 1;
        const Femme = 2;
    }

    class Phrase {
        public static $pronoms = array("j\'",'je ','tu ','il ','elle ','on ','nous ','vous ','ils ','elles ');
        public static $pronomsPlur = array('nous','vous','ils','elles');
        public static $pronomsSing = array('je','tu','on','il','elle');
        public static $pronomsFem = array('elle','elles');
        public static $pronomsHom = array('il','ils','on','nous','vous');
        public static $conjonctionCoord = array(' et ',' ou ',' ni ',' mais ',' or ',' car ',' donc ');
        public static $conjonctionSubordination = array(' ainsi ',' aussi ',' cependant ',' comme ',' lorsque ',' néanmoins ',
        ' puisque ',' quand ',' que ',' quoique ',' si ',' soit ',' toutefois ');
        public static $conjonctionLocution = array(' à ce que ', ' à condition que ', ' afin que ',' ainsi que ',' alors que ',
        'à mesure que','à moins que');
        public $contenu = NULL;
        public $type = TypesPhrases::Inconnu;
        public $sujet = NULL;
        public $sujetPlur = TypesPlur::Inconnu;
        public $sujetSexe = TypesSexe::Inconnu;
        public $verbe = NULL;
        public $verbe_id = NULL;
        public $mots = NULL;

        function __construct($phrase) {
            $this->contenu = strtolower($phrase);
            $this->contenu = str_replace('|','',$this->contenu);
        }

        function estVide(){
            if($this->contenu === NULL)
                return true;
            else
                return false;
        }

        function supprimerConjonction()
        {
            $this->contenu = str_replace(Phrase::$conjonctionCoord,' ',$this->contenu);
            $this->contenu = str_replace(Phrase::$conjonctionSubordination,' ',$this->contenu);
            $this->contenu = str_replace(Phrase::$conjonctionLocution,' ',$this->contenu);
        }

        function simplifierPhrase()
        {
            $temp = str_replace(Phrase::$pronoms,'',$this->contenu);
            $temp = str_replace(array('.','?','!',),'|',$temp);
            $temp = explode('|',$temp)[0];
            $this->contenu = $temp;
        }

        function trouverSujet()
        {
            foreach(Phrase::$pronoms as $sujet)
                if(strpos($this->contenu, $sujet) !== false)
                {
                    $this->sujet = $sujet;
                }
            return false;
        }

        function trouverPlur()
        {
            if(in_array($this->sujet,Phrase::$pronomsPlur))
                $this->sujetPlur = TypesPlur::Multiple;
            else if (in_array($this->sujet,Phrase::$pronomsSing))
                $this->sujetPlur = TypesPlur::Solo;
            else
                $this->sujetPlur = TypesPlur::Inconnu;
        }

        function trouverSexe()
        {
            if(in_array($this->sujet,Phrase::$pronomsPlur))
                $this->sujetSexe = TypesSexe::Homme;
            else if (in_array($this->sujet,Phrase::$pronomsHom))
                $this->sujetSexe = TypesSexe::Femme;
            else
                $this->sujetSexe = TypesSexe::Inconnu;
        }

        function identifierSujet()
        {
            $this->supprimerConjonction();
            $this->simplifierPhrase();
            $this->trouverSujet();
            $this->trouverSexe();
            $this->trouverPlur();
        }

        function trouverTypePhrase()
        {
            $normal = strpos($this->contenu,'.');
            $normal = ($normal != false) ? $normal : 9999;
            $exclamation = strpos($this->contenu,'!');
            $exclamation = ($exclamation != false) ? $exclamation : 9999;
            $question = strpos($this->contenu,'?');
            $normal = min(array($normal,$exclamation));
            if(($question!=false) && ($question < $normal))
            {
                $this->type = TypesPhrases::Question;
            }
            else
            {
                $this->type = TypesPhrases::Normal;
            }
        }
        
        function trouverMots()
        {
            $this->supprimerConjonction();
            $this->simplifierPhrase();
            $temp = $this->contenu;
            $temp = str_replace($this->sujet,'',$temp);
            $temp = str_replace($this->verbe,'',$temp);
            $this->mots = explode(' ',$temp);
        }
        
        function trouverPhrases($db)
        {
            $prepared = $db->prepare('SELECT * FROM BOTPHRASES WHERE mot IN (SELECT id FROM BOTWORDS WHERE word IN('.SQL_IN($this->mots).') AND type = ? ORDER BY RAND());');
            $prepared->execute(array($this->type));
            $rows = $prepared->fetchAll();
            if(mdArrayCount($rows)>0)
            {
                $str = $rows[0][2];
                echo $str;
            }
            else
            {
                $this->repondre($db);
            }
        }
        

        function trouverVerbe($db)
        {
            $this->simplifierPhrase();
            $temp = $this->contenu;
            $temp = explode(' ',$temp);
            $prepared = $db->prepare("SELECT id FROM BOTCONJUG WHERE conjug IN (".SQL_IN($temp).") LIMIT 1;");
            $prepared->execute();
            $rows = $prepared->fetchAll();
            if(mdArrayCount($rows)>0)
            {
                $id = $rows[0][0];
                $this->verbe = $id;
            }
        }

        function repondre($db)
        {
            if($this->verbe_id)
            {
                $prepared = $db->prepare('SELECT verb FROM BOTVERBS WHERE id ='.$this->verbe_id.' ');
                $prepared->execute();
                $rows = $prepared->fetchAll();
                if(mdArrayCount($rows)>0)
                {
                    $choice = rand(1,6);
                    switch($choice)
                    {
                        case 1:
                            echo "Le fait de ".$rows[0][0]." ne m'intéresse pas.";
                        break;

                        case 2:
                            echo "Pourquoi voudrais-je parler de ".$rows[0][0]." ?";
                        break;

                        case 3:
                            echo "D'accord, mais ".$rows[0][0]." n'est pas vraiment le propos.";  
                        break;

                        case 4:
                            echo "Vous voulez dire que je devrais ".$rows[0][0]." ?";
                        break;

                        case 5:
                            $prepared = $db->prepare('SELECT conjug FROM BOTCONJUG WHERE verbe ='.$this->verbe_id." AND temps = 1 ORDER BY id");
                            $prepared->execute();
                            $rows = $prepared->fetchAll();
                            if(mdArrayCount($rows)>0)
                            {
                                echo "Peut être que moi aussi je ".$rows[0][0].", qui sait ? ;)";
                            }
                        break;

                        case 6:
                            $prepared = $db->prepare('SELECT conjug FROM BOTCONJUG WHERE verbe ='.$this->verbe_id." AND temps = 1 ORDER BY id ASC");
                            $prepared->execute();
                            $rows = $prepared->fetchAll();
                            if(mdArrayCount($rows)>0)
                            {
                                echo "Vous ".$rows[3][0].", vous ?";
                            }
                        break;
                    }
                    
                }
            }
            else
            {
                $choice = rand(1,3);
                switch($choice)
                {
                    case 1:
                        echo "Je ne suis pas intéressé.";
                    break;

                    case 2:
                        echo "Pourrions-nous changer de sujet ?";
                    break;

                    case 3:
                        echo "Je suis à votre disposition si vous voulez parler d'autre chose.";  
                    break;
                }
            }
        }

        function trouverInfinitif($db)
        {
            $prepared = $db->prepare('SELECT verbe FROM BOTCONJUG WHERE id = ?');
            $prepared->execute(array($this->verbe));
            $rows = $prepared->fetchAll();
            if(mdArrayCount($rows)>0)
            {
                $id = $rows[0][0];
                $this->verbe_id = $id;
            }
        }
    }

    /// CLASSES>

    /// <FUNCTIONS
    /// FUNCTIONS>

    /// <MAIN
    try {
        $db = new PDO('mysql:host=localhost;dbname=bot', 'root', 'root');
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        if(isset($_POST['msg']))
        {
            if(strcmp($_POST['msg'],"<3")==0)
                echo '♥';
            else if(strcmp($_POST['msg'],"NUITDELINFO")==0)
                echo "Il est 04:38, j'ai sommeil; aled.";
            else if(strcmp($_POST['msg'],"Je devrais faire attention, je pourrais faire du mal aux autres.") == 0)
                echo "Bravo, vous êtes clean !";
            else
            {
                $test = new Phrase(addslashes(trim($_POST['msg'])));
                $test->trouverTypePhrase();
                $test->trouverVerbe($db);
                $test->trouverInfinitif($db);
                $test->trouverMots();
                $test->trouverPhrases($db);
            }
        }
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage() . "<br/>";
        die();
    }
    // MAIN>
?>